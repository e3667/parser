from urllib import request                        # For work with Internet
from bs4 import BeautifulSoup
import time					  # Just for fun, nothing useful for this prog
import csv, sys


def get_html(url):								# This function for reading the page's html code
    global headers
    response = request.urlopen(request.Request(url, headers=headers))
    return response.read()


def parse(html):                                                    # This one is the main, heart of programme
    global flag
    try:
        soup = BeautifulSoup(html)
        table = soup.find('table', class_='table table-users')			# Firstly, we read the table of users
        users = []
        for row in table.find_all('tr')[1:]:			    # Secondary, we going through rows
            cols = row.find_all('td')						# Each element is containing in its own column
            users.append({
                'Name': cols[0].div.p.a.text,					# Attribute 'text' - for reading what's inside of tag
                'Power': round(float(cols[2].text), 2),
                'Rating': round(float(cols[3].strong.text), 2),
            })
        return users
    except:
        flag = 0
        return


def save(users, path):
    with open(path, 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(('Имя Пользователя', 'Сила', 'Рейтинг'))

        writer.writerows((user['Name'], user['Power'], user['Rating']) for user in users)


start = time.time()
headers = dict()
headers['user-agent'] = '''Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko)
                  Chrome/75.0.3770.100 Safari/537.36 OPR/62.0.3331.72 (Edition 360-1)'''      # Against robot-protocol

current_page, flag, users, last = 1, 1, [], 1
while flag and current_page < 16:                       # For normal just get rid of the second condition
    last = current_page
    base_url = 'https://tabun.everypony.ru/people/index/page' + str(current_page) + '/'	 # Every time url's different
    try:
        users.extend(parse(get_html(base_url)))                           # Don't know why, but otherwise it didn't work
        print(*[user for user in users], sep='\n')
        print('--------------------------------------------------------')
        print('               THAT WAS PAGE № %d' % current_page)
        print('--------------------------------------------------------')
    except:
        print(sys.exc_info()[1])
        break
    current_page += 1

save(users, 'users.csv')
print('%d is all pages we can find' % last)
print("Those were all users!")
print(time.time()-start)
